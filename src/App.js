import "./App.css";
import Navbar from "./components/Navbar";
import CreatePlaylist from "./components/CreatePlaylist";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PlaylistList from "./components/PlaylistList";

function App() {
  return (
    <>

        <Navbar />
        <Routes>
          <Route path="/" element={<PlaylistList />}></Route>
          <Route index element={<PlaylistList />}></Route>
          <Route path="/playlists" element={<PlaylistList />}></Route>
          <Route path="/addPlaylist" element={<CreatePlaylist />}></Route>
        </Routes>
      
    </>
  );
}

export default App;
