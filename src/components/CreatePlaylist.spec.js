import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import CreatePlaylist from "./CreatePlaylist";
import axios from "axios";
jest.mock("axios");
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import axiosMock from "axios"
// import '@testing-library/jest-dom'

jest.mock("axios")

describe("CreatePlaylist rendering", () => {
  test("renders correctly", () => {
    render(
      <Router>
        <CreatePlaylist />
      </Router>
    );
    const h1Element = screen.getByTestId("create-playlist-heading");
    expect(h1Element).toBeInTheDocument();

    const createPlaylistButton = screen.getByRole("button");
    expect(createPlaylistButton).toBeInTheDocument();

    const nameInputElement = screen.getByText("Name");
    expect(nameInputElement).toBeInTheDocument();

    const divHeadingElement = screen.getByTestId("div-heading");
    expect(divHeadingElement).toBeInTheDocument();

    const divName = screen.getByTestId("div-name");
    expect(divName).toBeInTheDocument();

    const divButton = screen.getByTestId("div-button");
    expect(divButton).toBeInTheDocument();
  });
});


  test("save button functionality", async () => {
    const {getByTestId} = render(
        <Router>
          <CreatePlaylist />
        </Router>
      );
    axiosMock.post.mockResolvedValueOnce({
      data: {id: 1,
      name: "Playlist Name"}
    })
const button = getByTestId("button");

fireEvent.click(button);

// await waitFor(() => expect(axiosMock.post).toHaveBeenCalledTimes(1));
 expect(axiosMock.post).toHaveBeenCalledTimes(1)

// expect(screen.findByText('clicked')).toBeInTheDocument();
});
// describe("Save button functionality", () => {
//   // it("", () => {});
//   const savePlaylist = jest.fn();
//   const {queryByText} = render(<AddPlaylist/>);
//   const button = queryByText("Save");

//   fireEvent.click(button);

//   expect(savePlaylist).toHaveBeenCalledTimes(1);
// });

// describe("AddPlaylist functionality", () => {
//   it("should be able to type in input",() => {
//     render(<Router>
//       <AddPlaylist />
//     </Router>);
//   })
//   const nameElement = screen.getByTestId("name");
//   fireEvent.change(nameElement, {target: {value: "New Name"}});
//   expect(nameElement.value).toBe("New Name");

// })
