import PlaylistList from "./PlaylistList";
import { render, screen,fireEvent, waitFor } from "@testing-library/react";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import axios from 'axios'
// import axiosMock from "axios"
// import '@testing-library/jest-dom'

// jest.mock("axios")

if(typeof process != 'undefined'){
  axios.defaults.adapter = require('axios/lib/adapters/http');
}

describe("PlaylistList rendering", () => {
  test("renders correctly", () => {
    render(
      <Router>
        <PlaylistList />
      </Router>
    );

    const h1Element = screen.getByTestId("main-heading");
    expect(h1Element).toBeInTheDocument();

    const nameHeading = screen.getByTestId("name-heading");
    expect(nameHeading).toBeInTheDocument();

    const songsHeading = screen.getByTestId("songs-heading");
    expect(songsHeading).toBeInTheDocument();

    const actionsHeading = screen.getByTestId("actions-heading");
    expect(actionsHeading).toBeInTheDocument();

    const tableElement = screen.getByRole("table");
    expect(tableElement).toBeInTheDocument();

    const createPlaylistElement = screen.getByTestId("create-playlist");
    // });
    expect(createPlaylistElement).toBeInTheDocument();
  });
});

describe("PlaylistList rendering", () => {
  test("renders correctly", async () => {
    render(
      <Router>
        <PlaylistList />
      </Router>
    );

    // act(() => {
    // const deletePlaylistElement = await screen.findAllByTestId("delete-playlist");
    const deletePlaylistElement = await screen.findAllByTestId("delete-playlist");
    // });
    expect(deletePlaylistElement.length).toBe(1);

    const addSongElement = await screen.findAllByTestId("add-song");
    // });
    expect(addSongElement.length).toBe(1);

    const removeSongElement = await screen.findAllByTestId("remove-song");
    // });
    expect(removeSongElement.length).toBe(3);
  });
});

// describe("PlaylistList functionality for delete playlist", () => {
//   test("delete playlist functions correctly", async () => {
//     render(
//       <Router>
//         <PlaylistList />
//       </Router>
//     );
//     axiosMock.delete.mockResolvedValueOnce({
//       data: {status: 200}
//     })
//     const deletePlaylistElement = await screen.findAllByTestId("delete-playlist");

//     fireEvent.click(deletePlaylistElement[0]);

//     expect(axiosMock.delete).toHaveBeenCalledTimes(1)

//     // const deletePlaylistElement = await screen.findAllByTestId("delete-playlist");
//     // fireEvent.click(deletePlaylistElement);
//   });
// });






    // const createPlaylistElement = screen.getByTestId("create-playlist");
    // fireEvent.click(createPlaylistElement);

    // const createPlaylistHeading =  await screen.findByTestId("div-name");

    // expect(createPlaylistHeading).toBeInTheDocument();
 
