import React from "react";
import "./Navbar.css"

const Navbar = () => {
  return (
    <div data-testid="div1" className="bg-gray-800">
      <div data-testid="div2" className="h-20 flex items-center">
        <p className="nav-heading">Music Player App</p>
      </div>
    </div>
  );
};

export default Navbar;
