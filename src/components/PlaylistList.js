import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import PlaylistService from "../services/PlaylistService";
import SongService from "../services/SongService";
import "./PlaylistList.css";
import AddSongModal from "./AddSongModal";

function PlaylistList() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [playlists, setPlaylists] = useState([]);
  const [songs, setSongs] = useState([]);

  const [showModal, setShowModal] = useState(false);
  const [playlistToAddSongTo, setPlaylistToAddSongTo] = useState(null);
  const [alreadyAddedSongs, setAllreadyAddedSongs] = useState([]);
  const [playlistName, setPlaylistName] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const responseOne = await PlaylistService.getPlaylists();
        const responseTwo = await SongService.getSongs();
        setPlaylists(responseOne.data);
        setSongs(responseTwo.data);
        console.log("playlists", playlists);
        console.log("songs", songs);
      } catch (error) {
        // console.log(error);
      }
      setLoading(false);
    };
    fetchData();
  }, []);
  const handleClose = () => {
    setShowModal(false);
    setPlaylistName(null);
    setPlaylistToAddSongTo(null);
    setAllreadyAddedSongs(null);
  };
  const deletePlaylist = (e, id) => {
    e.preventDefault();
    PlaylistService.deletePlaylist(id).then((res) => {
      console.log(res);
      if (playlists) {
        setPlaylists((prevElement) => {
          return prevElement.filter((playlist) => playlist.playlistId !== id);
        });
      }
    });
  };
  const deleteSong = (e, playlistId, songId) => {
    e.preventDefault();
    PlaylistService.deleteSong(playlistId, songId).then((res) => {
      console.log(res);
      // if (songs) {
      //   console.log("songs in delete",songs)
      //   setSongs((prevElement) => {
      //     return prevElement.filter((song) => song.songId !== songId);
      //   });
      // }
      window.location.reload();
    });
  };

  const addSong = (e, playlistId, songs, name) => {
    setAllreadyAddedSongs(songs);
    setShowModal(true);
    setPlaylistToAddSongTo(playlistId);
    console.log("haha", playlistId, e, songs);
    setPlaylistName(name);
  };

  return (
    <>
      <div className="heading">
        <button
          data-testid="create-playlist"
          className="general-buttons"
          onClick={() => navigate("/addPlaylist")}
        >
          Create playlist
        </button>
      </div>
      <div className="show-playlist">
        <div>
          <h1 className="heading" data-testid="main-heading">
            List of playlists
          </h1>
        </div>

        <div className="table">
          <table className="playlist-table" role="table">
            <thead className="playlist-table-head">
              <tr>
                <th
                  className="playlist-table-colheader1"
                  data-testid="name-heading"
                >
                  Name
                </th>
                <th
                  className="playlist-table-colheader2"
                  data-testid="songs-heading"
                >
                  Songs
                </th>
                <th
                  className="playlist-table-colheader3"
                  data-testid="actions-heading"
                >
                  Actions
                </th>
              </tr>
            </thead>
            {!loading && (
              <tbody>
                {playlists.map((playlist) => (
                  <tr key={playlist.playlistId}>
                    <td className="table-body-playlist-name">
                      {playlist.name}
                    </td>
                    <td>
                      {playlist.songs.map((song) => (
                        <div className="table-body-songs">
                          <div className="table-body-songs-title">
                            {song.title}
                          </div>
                          <a
                            data-testid="remove-song"
                            className="general-buttons"
                            onClick={(e, id1, id2) => {
                              // setSongs(playlist.songs)
                              // console.log("songsI",playlist.songs)
                              return deleteSong(
                                e,
                                playlist.playlistId,
                                song.songId
                              );
                            }}
                          >
                            X
                          </a>
                          {/* {console.log("songs",songs)} */}
                          {/* <a href="#">Delete song</a> */}
                        </div>
                      ))}
                    </td>
                    {/* {console.log(
                    "print playlist",
                    playlist.playlistId,
                    playlist.songs
                  )} */}
                    <td className="table-body-actions">
                      {/* <a href="#">View songs</a> */}
                      <a
                        data-testid="add-song"
                        className="general-buttons"
                        onClick={(e, id) =>
                          addSong(
                            e,
                            playlist.playlistId,
                            playlist.songs,
                            playlist.name
                          )
                        }
                      >
                        Add song
                      </a>
                      {/* <a href="#">Delete song</a> */}
                      <a
                        data-testid="delete-playlist"
                        className="general-buttons"
                        key={playlist.id}
                        onClick={(e, id) =>
                          deletePlaylist(e, playlist.playlistId)
                        }
                      >
                        Delete playlist
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            )}
          </table>
        </div>
      </div>
      {showModal ? (
        <AddSongModal
          songs={songs}
          playlistToAddSongTo={playlistToAddSongTo}
          alreadyAddedSongs={alreadyAddedSongs}
          playlistName={playlistName}
          handleClose={handleClose}
        />
      ) : null}
    </>
  );
}
export default PlaylistList;
