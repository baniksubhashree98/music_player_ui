import React from "react";
// import { useNavigate } from "react-router-dom";
import "./AddSongModal.css"
import PlaylistService from "../services/PlaylistService";

function AddSongModal({songs,playlistToAddSongTo,alreadyAddedSongs,playlistName, handleClose}) {
    console.log("songs",songs);
    console.log("already added",alreadyAddedSongs);

    // const getDifference=(array1, array2) => {
    //     return array1.filter(object1 => {
    //       return !array2.some(object2 => {
    //         return object1.id === object2.id;
    //       });
    //     });
    //   };
    // console.log("Get diff",getDifference(songs, alreadyAddedSongs));
    const results=songs.filter(({songId:id1}) => !alreadyAddedSongs.some(({songId:id2})=>id2===id1));
    let len = results.length;
    console.log("results",results)

    const addSong = (e, song) => {
      e.preventDefault();
      PlaylistService.addSong(playlistToAddSongTo, song).then((res) => {
        console.log(res);
        // if (songs) {
        //   console.log("songs in delete",songs)
        //   setSongs((prevElement) => {
        //     return prevElement.filter((song) => song.songId !== songId);
        //   });
        // }
        window.location.reload();
      });
    };

  return (
    <div className="modal-backdrop">
    {/* <h2>{playlistToAddSongTo}</h2> */}
    <div className="modal">
    <h2>{playlistName}</h2>
      {(len>0) && results.map((result) => {
        return (
        <>
            <p key={result.id}>{result.title}</p>
            <button class="button-8" onClick={(e)=>addSong(e,result)}>Add song</button>
        </>);
      })}
      {(len==0) && <p>You have added all songs to this playlist!</p>}
      <br/>
      <br/>
      <button class="general-buttons" onClick={handleClose}>Close window</button>
    </div>
    
    </div>
  );
}

export default AddSongModal;

/*

*/
