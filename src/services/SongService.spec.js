import { when } from "jest-when";
import SongService from "./SongService";
import apiService from "../helpers/apiService";

jest.mock("../helpers/apiService");

describe("Song Service", () => {
  it("should return all songs", async () => {
    const data = [
      {
        songId: 1,
        title: "song1",
        artist: "dummy artist"
      },
      {
        songId: 2,
        title: "song2",
        artist: "dummy artist"
      },
    ];

    apiService.get.mockResolvedValue({ data: data });
    const songs = await SongService.getSongs();

    expect(songs.data).toHaveLength(2);

    expect(songs.data).toEqual([
      {
        songId: 1,
        title: "song1",
        artist: "dummy artist"
      },
      {
        songId: 2,
        title: "song2",
        artist: "dummy artist"
      },
    ]);
  });
});